package kz.vainshtein.restjsonsample.client.entity;

import jakarta.persistence.*;
import kz.vainshtein.restjsonsample.client.security.Roles;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Entity
@Table(name = "AUTHORITIES")
@IdClass(AuthorityId.class)
@Getter
@Setter
@Accessors(chain = true)
public class Authority implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERNAME")
    private User user;

    @Id
    @Column(name = "AUTHORITY")
    @Enumerated(EnumType.STRING)
    private Roles role;
}
