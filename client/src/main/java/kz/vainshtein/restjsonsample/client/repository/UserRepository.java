package kz.vainshtein.restjsonsample.client.repository;

import kz.vainshtein.restjsonsample.client.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {

    @Query("select u from User u left join fetch u.authorities a order by u.username")
    List<User> findAllUsersAndAuthorities();
}
