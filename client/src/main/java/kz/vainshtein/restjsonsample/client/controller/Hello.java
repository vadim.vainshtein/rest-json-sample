package kz.vainshtein.restjsonsample.client.controller;

import kz.vainshtein.restjsonsample.client.service.CoffeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class Hello {

    private final CoffeeService coffeeService;

    @GetMapping("/hello")
    public String hello() {

        var authentication = SecurityContextHolder.getContext()
                                                  .getAuthentication();

        var coffee = coffeeService.getCoffee();
        var brand = coffee.getBrand();

        return "hello";
    }
}
