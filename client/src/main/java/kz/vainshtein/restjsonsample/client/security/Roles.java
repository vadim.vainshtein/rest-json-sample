package kz.vainshtein.restjsonsample.client.security;

import lombok.Getter;

@Getter
public enum Roles {
    ROLE_ADMIN,
    ROLE_USER
}
