package kz.vainshtein.restjsonsample.client.service;

import kz.vainshtein.restjsonsample.common.entity.Coffee;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;


@Service
@RequiredArgsConstructor
public class CoffeeService {

    private final WebClient webClient;

    public Coffee getCoffee() {
        return webClient.get()
                        .uri("/coffee")
                        .retrieve()
                        .bodyToMono(Coffee.class)
                        .block();
    }
}
