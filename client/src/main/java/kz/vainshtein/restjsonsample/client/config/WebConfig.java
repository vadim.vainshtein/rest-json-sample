package kz.vainshtein.restjsonsample.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebConfig {

    @Value("${coffee-server-url}")
    private String serverUrl;

    @Bean
    public WebClient webClient() {
        return WebClient.builder()
                        .baseUrl(serverUrl)
                        .build();
    }
}
