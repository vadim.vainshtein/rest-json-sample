package kz.vainshtein.restjsonsample.client.entity;


import kz.vainshtein.restjsonsample.client.security.Roles;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AuthorityId implements Serializable {
    private User user;
    private Roles role;
}
