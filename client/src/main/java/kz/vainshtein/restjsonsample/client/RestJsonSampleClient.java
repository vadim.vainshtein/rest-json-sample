package kz.vainshtein.restjsonsample.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestJsonSampleClient {

    public static void main(String[] args) {
        SpringApplication.run(RestJsonSampleClient.class, args);
    }
}
