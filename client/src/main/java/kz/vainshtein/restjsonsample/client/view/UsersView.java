package kz.vainshtein.restjsonsample.client.view;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import jakarta.annotation.security.RolesAllowed;
import kz.vainshtein.restjsonsample.client.entity.User;
import kz.vainshtein.restjsonsample.client.service.UserService;

import static java.util.Objects.nonNull;

@Route("/users")
@RolesAllowed("ROLE_ADMIN")
public class UsersView extends VerticalLayout {

    private final UserService userService;

    private final Grid<User> usersGrid = new Grid<>(User.class);
    private final Button create = new Button("Create...", event -> createUser());
    private final UserForm userForm = new UserForm();

    public UsersView(UserService userService) {
        this.userService = userService;

        configureDataGrid();
        configureUserForm();

        var content = new HorizontalLayout(usersGrid, userForm);

        content.setFlexGrow(2, usersGrid);
        content.setFlexGrow(1, this.userForm);
        content.setSizeFull();

        add(create, content);

        closeEditor();
    }

    private void configureUserForm() {
        userForm.addSaveListener(event -> {
            userService.save(event.getUser());
            updateGrid();
        });

        userForm.addDeleteListener(event -> {
            userService.delete(event.getUser());
            updateGrid();
        });
    }

    private void configureDataGrid() {
        usersGrid.setColumns("username", "enabled");
        usersGrid.addColumn(userService::getAllUserAuthorities)
                 .setHeader("Authorities");
        usersGrid.asSingleSelect()
                 .addValueChangeListener(event -> editUser(event.getValue()));
        updateGrid();
    }

    private void updateGrid() {
        usersGrid.setItems(userService.findAll());
    }

    private void editUser(User user) {
        if (nonNull(user)) {
            userForm.setUser(user);
            userForm.setVisible(true);
        } else {
            userForm.setVisible(false);
        }
    }

    private void closeEditor() {
        userForm.setUser(null);
        userForm.setVisible(false);
    }

    private void createUser() {
        usersGrid.asSingleSelect()
                 .clear();
        editUser(new User());
    }
}
