package kz.vainshtein.restjsonsample.client.view;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.checkbox.CheckboxGroupVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import kz.vainshtein.restjsonsample.client.entity.Authority;
import kz.vainshtein.restjsonsample.client.entity.User;
import kz.vainshtein.restjsonsample.client.security.Roles;
import lombok.Getter;

import java.util.Set;
import java.util.stream.Collectors;

public class UserForm extends FormLayout {


    private final TextField username = new TextField("Username");
    private final TextField password = new TextField("Password");
    private final Checkbox enabled = new Checkbox("Enabled");
    private final CheckboxGroup<Roles> roles = new CheckboxGroup<>();

    private Binder<User> binder = new Binder<>(User.class);

    private final Button save = new Button("Save", event -> fireEvent(new SaveEvent(this, binder.getBean())));
    private final Button delete = new Button("Delete", event -> fireEvent(new DeleteEvent(this, binder.getBean())));

    public UserForm() {

        binder.forField(roles)
                .bind(
                        UserForm::getUserRoles,
                        this::setUserRoles);
        binder.bindInstanceFields(this);

        configureRolesCheckboxGroup();

        add(new VerticalLayout(username, password, enabled, roles), getButtonsPanel());
    }

    private void configureRolesCheckboxGroup() {
        roles.setItems(Roles.values());
        roles.setItemLabelGenerator(Roles::name);
        roles.addThemeVariants(CheckboxGroupVariant.LUMO_VERTICAL);
    }

    private HorizontalLayout getButtonsPanel() {
        return new HorizontalLayout(save, delete);
    }

    private void setUserRoles(User user, Set<Roles> roleSet) {
        var authorities = roleSet.stream()
                .map(role -> createAuthority(user, role))
                .collect(Collectors.toSet());
        user.setAuthorities(authorities);
    }

    private static Authority createAuthority(User user, Roles role) {
        var authority = new Authority();
        authority.setRole(role);
        authority.setUser(user);
        return authority;
    }

    private static Set<Roles> getUserRoles(User user) {
        return user.getAuthorities()
                .stream()
                .map(Authority::getRole)
                .collect(Collectors.toSet());
    }

    public void setUser(User user) {
        binder.setBean(user);
    }

    public Registration addSaveListener(ComponentEventListener<SaveEvent> listener) {
        return addListener(SaveEvent.class, listener);
    }

    public Registration addDeleteListener(ComponentEventListener<DeleteEvent> listener) {
        return addListener(DeleteEvent.class, listener);
    }

    public Registration addCloseListener(ComponentEventListener<CloseEvent> listener) {
        return addListener(CloseEvent.class, listener);
    }

    public abstract static class UserFormEvent extends ComponentEvent<UserForm> {

        @Getter
        private User user;

        UserFormEvent(UserForm source, User user) {
            super(source, false);
            this.user = user;
        }
    }

    public static class SaveEvent extends UserFormEvent {

        public SaveEvent(UserForm source, User user) {
            super(source, user);
        }
    }

    public static class DeleteEvent extends UserFormEvent {

        public DeleteEvent(UserForm source, User user) {
            super(source, user);
        }
    }

    public static class CloseEvent extends UserFormEvent {

        public CloseEvent(UserForm source, User user) {
            super(source, user);
        }
    }
}
