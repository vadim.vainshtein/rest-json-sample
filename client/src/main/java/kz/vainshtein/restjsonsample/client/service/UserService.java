package kz.vainshtein.restjsonsample.client.service;

import kz.vainshtein.restjsonsample.client.entity.Authority;
import kz.vainshtein.restjsonsample.client.entity.User;
import kz.vainshtein.restjsonsample.client.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public List<User> findAll() {
        return userRepository.findAllUsersAndAuthorities();
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

    public String getAllUserAuthorities(User user) {
        return user.getAuthorities()
                   .stream()
                   .map(Authority::getRole)
                   .sorted()
                   .map(Enum::name)
                   .collect(Collectors.joining(", "));
    }
}
