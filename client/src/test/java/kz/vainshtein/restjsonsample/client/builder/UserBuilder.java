package kz.vainshtein.restjsonsample.client.builder;

import kz.vainshtein.restjsonsample.client.entity.Authority;
import kz.vainshtein.restjsonsample.client.entity.User;

import java.util.HashSet;
import java.util.Set;

public class UserBuilder {

    private String username = "sample_user";
    private String password = "samplePassword";
    private Boolean enabled = true;
    private Set<Authority> authorities = new HashSet<>();

    private UserBuilder() {
    }

    public static UserBuilder aUser() {
        return new UserBuilder();
    }

    public UserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public UserBuilder withAuthority(AuthorityBuilder authorityBuilder) {
        authorities.add(authorityBuilder.build());
        return this;
    }

    public User build() {
        var user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEnabled(enabled);
        user.setAuthorities(authorities);

        authorities.forEach(authority -> authority.setUser(user));

        return user;
    }
}
