package kz.vainshtein.restjsonsample.client.builder;

import kz.vainshtein.restjsonsample.client.entity.Authority;
import kz.vainshtein.restjsonsample.client.entity.User;
import kz.vainshtein.restjsonsample.client.security.Roles;

import static kz.vainshtein.restjsonsample.client.builder.UserBuilder.aUser;

public class AuthorityBuilder {

    private Roles role = Roles.ROLE_USER;
    private User user = aUser().build();

    private AuthorityBuilder() {
    }

    public static AuthorityBuilder anAuthority() {
        return new AuthorityBuilder();
    }

    public AuthorityBuilder withRole(Roles role) {
        this.role = role;
        return this;
    }

    public AuthorityBuilder with(UserBuilder userBuilder) {
        this.user = userBuilder.build();
        return this;
    }

    public Authority build() {
        var authority = new Authority().setRole(role)
                                       .setUser(user);
        user.getAuthorities()
            .add(authority);

        return authority;
    }
}
