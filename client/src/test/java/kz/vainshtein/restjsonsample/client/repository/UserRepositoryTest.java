package kz.vainshtein.restjsonsample.client.repository;

import kz.vainshtein.restjsonsample.client.entity.Authority;
import kz.vainshtein.restjsonsample.client.security.Roles;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static kz.vainshtein.restjsonsample.client.builder.AuthorityBuilder.anAuthority;
import static kz.vainshtein.restjsonsample.client.builder.UserBuilder.aUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void save_returnsSavedEntity() {

        var user = aUser().build();
        assertEquals(user, userRepository.save(user));
    }

    @Test
    void findAllUsersAndAuthorities_returnsAuthorities() {

        var authority = anAuthority().withRole(Roles.ROLE_USER)
                                     .build();
        var user = authority.getUser();
        entityManager.persist(user);

        var loadedUsers = userRepository.findAllUsersAndAuthorities();

        assertEquals(
                authority.getRole(),
                loadedUsers.stream()
                           .filter(u -> u.getUsername()
                                         .equals(user.getUsername()))
                           .findFirst()
                           .orElseThrow()
                           .getAuthorities()
                           .stream()
                           .findFirst()
                           .map(Authority::getRole)
                           .orElseThrow());
    }

    @Test
    void findAllUsersAndAuthorities_worksWithEmptyAuthorities() {

        var user = aUser().build();
        entityManager.persist(user);

        assertTrue(userRepository.findAllUsersAndAuthorities()
                                 .contains(user));
    }
}