package kz.vainshtein.restjsonsample.client.service;

import kz.vainshtein.restjsonsample.client.repository.UserRepository;
import kz.vainshtein.restjsonsample.client.security.Roles;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static kz.vainshtein.restjsonsample.client.builder.AuthorityBuilder.anAuthority;
import static kz.vainshtein.restjsonsample.client.builder.UserBuilder.aUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void findAllUsersAndAuthorities() {

        var users = List.of(
                aUser().withUsername("user1")
                       .build(),
                aUser().withUsername("user2")
                       .build());

        when(userRepository.findAllUsersAndAuthorities()).thenReturn(users);

        var actualUsers = userService.findAll();

        assertEquals(users, actualUsers);
    }

    @Test
    void getAuthorities() {

        var user = aUser().withAuthority(anAuthority().withRole(Roles.ROLE_USER))
                          .withAuthority(anAuthority().withRole(Roles.ROLE_ADMIN))
                          .build();

        var rolesString = userService.getAllUserAuthorities(user);

        assertEquals("ROLE_ADMIN, ROLE_USER", rolesString);
    }
}