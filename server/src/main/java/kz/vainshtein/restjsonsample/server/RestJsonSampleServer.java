package kz.vainshtein.restjsonsample.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestJsonSampleServer {

	public static void main(String[] args) {
		SpringApplication.run(RestJsonSampleServer.class, args);
	}

}
