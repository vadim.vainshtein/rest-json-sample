package kz.vainshtein.restjsonsample.server.controller;

import kz.vainshtein.restjsonsample.common.entity.Brand;
import kz.vainshtein.restjsonsample.common.entity.Coffee;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
public class CoffeeController {

    @GetMapping("/coffee")
    public Coffee getCoffee() {

        var brand = new Brand().setName("Coffee firm")
                               .setSince(LocalDate.of(2022, 1, 1));
        return new Coffee().setBrand(brand)
                           .setName("Some coffee")
                           .setDate(LocalDateTime.now());
    }
}
