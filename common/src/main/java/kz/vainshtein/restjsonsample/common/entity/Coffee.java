package kz.vainshtein.restjsonsample.common.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
public class Coffee {
    private String name;
    private Brand brand;
    private LocalDateTime date;
}