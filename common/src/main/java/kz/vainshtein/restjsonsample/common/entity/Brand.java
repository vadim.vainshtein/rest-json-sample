package kz.vainshtein.restjsonsample.common.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Getter
@Setter
@Accessors(chain = true)
public class Brand {
    private String name;
    private LocalDate since;
}
